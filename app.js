const bodyParser = require('body-parser');
const request = require('request');
const app = require('express')();
const server = require('http').Server(app);
const io = require('socket.io')(server);

const $host = 'http://mmw.moczniak.usermd.net/api/inGame';
const $getUserIDRouteUrl = $host + '/userIDFromToken?token=';
const $getRoomPermission = $host + '/chatRoomPermission/{roomID}?token=';

server.listen(3900);

app.use( bodyParser.json() );
app.use(bodyParser.urlencoded({
  extended: true
}));

app.get('/', (req, res) => {
  res.send('raus!!');
});

app.post('/newEvent', (req, res) => {
  res.send('ok');
  io.to('user_' + req.body.userID).emit('newEvent', { type: req.body.data.type, dataID: req.body.data.dataID });
});

app.post('/newChatMessage', (req, res) => {
  res.send('ok');
  io.to('room_' + req.body.room).emit('newChatMessage', { data: req.body });
});

io.on('connection', (socket) => {
  console.log('connection: ' + socket.id);
  socket.on('joinEventRoom', (data) => {
    request( '' + $getUserIDRouteUrl + data.userToken,  (error, response, body) => {
      if (!error && response.statusCode == 200) {
        const data = JSON.parse(body);
        socket.join('user_' + data.userID);
      }
    });
  });

  socket.on('leaveEventRoom', (data) => {
    request( '' + $getUserIDRouteUrl + data.userToken,  (error, response, body) => {
      if (!error && response.statusCode == 200) {
        const data = JSON.parse(body);
        socket.leave('user_' + data.userID);
      }
    });
  });

  socket.on('joinChatRoom', (data) => {
    request( '' + $getRoomPermission.replace("{roomID}", data.roomID) + data.userToken,  (error, response, body) => {
      if (!error && response.statusCode == 200) {
        const parsed = JSON.parse(body);
        if (parsed.data === true) {
          socket.join('room_' + data.roomID);
        }
      }
    });
  });

});
